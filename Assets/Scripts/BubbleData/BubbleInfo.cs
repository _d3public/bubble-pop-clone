using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BubbleInfo", menuName = "Skin/BubbleInfo")]
public class BubbleInfo : ScriptableObject
{
    public Mesh BubbleMesh;
    public float TextZValue;

    [Space(20f)]
    public int[] Numbers = new int[] { 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048 };
    public Color[] Colors;
}
