using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameScreenUI : MonoBehaviour
{
    [SerializeField] Slider LevelBarSlider;
    [SerializeField] TextMeshProUGUI scoreText;
    [SerializeField] TextMeshProUGUI currentLevelText, nextLevelText;
    private float currentPercentage;

    private Coroutine levelBarRoutine;
    Dictionary<int, string> valueMap = new Dictionary<int, string>()
    {
        {3,"K"},
        {6,"M"},
        {9,"B"},
        {12,"T"},
    };
    internal void SetScore(float score)
    {
        var scoreText = "";
        int pow = GetPowerOfValue(score);

        if (pow > 0)
            scoreText = $"{ScoreDivision(score, 10, pow).ToString("0.00")}{valueMap[pow]}";
        else
            scoreText = $"{(int)score}";

        this.scoreText.text = scoreText;
    }

    internal void UpdateLevelText(int currentLevel)
    {
        currentLevelText.text = (currentLevel + 1).ToString();
        nextLevelText.text = (currentLevel + 2).ToString();
    }

    int GetPowerOfValue(float value)
    {
        int powValue = Mathf.FloorToInt(Mathf.Log10(value));
        int rest = powValue % 3;

        if (rest == 0)
            return powValue;
        else
            return powValue - rest;

    }
    float ScoreDivision(float value, int _base, int pow)
    {
        float temp = Mathf.Exp(Mathf.Log(value) - Mathf.Log(Mathf.Pow(_base, pow)));
        return temp;

    }
    internal void UpdateLevelBar(float percentage, bool immediate)
    {
        if (immediate)
        {
            SetLevelBar(percentage);
        }
        else
        {
            if (levelBarRoutine != null)
                StopCoroutine(levelBarRoutine);
            levelBarRoutine = StartCoroutine(PercentageRoutine(percentage, 1f));
        }
    }
    IEnumerator PercentageRoutine(float endValue, float speed)
    {
        var initialVal = 0f;
        var initialPerc = currentPercentage;
        while (true)
        {
            initialVal += Time.deltaTime * speed;
            SetLevelBar(Mathf.Lerp(initialPerc, endValue, Mathf.Clamp01(initialVal)));
            if (initialVal >= 1f)
                break;
            yield return new WaitForEndOfFrame();
        }
    }
    void SetLevelBar(float perc)
    {
        currentPercentage = perc;
        LevelBarSlider.value = currentPercentage;
    }
}
