using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelData", menuName = "GameData/LevelData")]
public class LevelData : ScriptableObject
{
    public List<LevelDataValues> LevelDataValues;
    public float[] TargetLevelScores;
}
[System.Serializable]
public struct LevelDataValues
{
    public int MinBubbleIndex;
    public int MaxBubbleIndex;
    public int GameAreaMinBubble;
    public int GameAreaMaxBubble;
}
