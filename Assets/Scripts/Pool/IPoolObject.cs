﻿using UnityEngine;

public interface IPoolObject
{
    GameObject KeyObject
    {
        get;
        set;
    }
    GameObject ToGameObject();
}
