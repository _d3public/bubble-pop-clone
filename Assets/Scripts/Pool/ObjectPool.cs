﻿
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace TinyD3.ObjectPool
{
    public class ObjectPool<T> where T : IPoolObject
    {
        private Transform TContainer;

        Vector3 initialPos = Vector3.one * 5000;

        Dictionary<GameObject, Queue<T>> deactiveObjectsMap = new Dictionary<GameObject, Queue<T>>();
        Dictionary<GameObject, List<T>> activeObjectsMap = new Dictionary<GameObject, List<T>>();

        public ObjectPool(Transform container)
        {
            TContainer = container;
        }
        public ObjectPool(Transform container, List<GameObject> objectList, int count)
        {
            TContainer = container;

            if (objectList != null)
            {
                CreateObjects(objectList, count);
            }
        }
        public ObjectPool(Transform container, GameObject obj, int count = 20)
        {
            TContainer = container;

            if (obj != null)
            {
                CreateObjects(obj, count);
            }
        }

        Dictionary<GameObject, int> necessaryObjCounts = new Dictionary<GameObject, int>();
        internal void ObjectCountController(List<GameObject> objects)
        {
            if (objects.Count > 0)
            {
                List<GameObject> controlledObjects = new List<GameObject>();
                foreach (GameObject lo in objects)
                {
                    if (!necessaryObjCounts.ContainsKey(lo))
                    {
                        necessaryObjCounts.Add(lo, objects.Count(o => o == lo));
                        controlledObjects.Add(lo);
                    }
                    else
                    {
                        if (!controlledObjects.Contains(lo))
                        {
                            controlledObjects.Add(lo);
                            int count = objects.Count(o => o == lo);
                            if (necessaryObjCounts[lo] < count)
                                necessaryObjCounts[lo] = count;
                        }
                    }
                }
            }
        }
        internal void CreateObjects()
        {
            if (necessaryObjCounts.Keys.Count > 0)
            {
                foreach (GameObject go in necessaryObjCounts.Keys)
                {
                    InstantiateObject(go, necessaryObjCounts[go]);
                }
            }
        }
        internal void CreateObjects(List<GameObject> objectList, int count)
        {
            foreach (GameObject g in objectList)
            {
                InstantiateObject(g, count);
            }
        }
        internal void CreateObjects(GameObject obj, int count)
        {
            InstantiateObject(obj, count);
        }

        private void InstantiateObject(GameObject go, int count)
        {
            if (!deactiveObjectsMap.ContainsKey(go))
            {
                deactiveObjectsMap.Add(go, new Queue<T>());
                for (int i = 0; i < count; i++)
                {
                    GameObject tempObj = MonoBehaviour.Instantiate(go, initialPos, Quaternion.identity, TContainer);
                    tempObj.SetActive(false);
                    tempObj.transform.localPosition = initialPos;
                    //deactiveObjectsMap[go].Enqueue((T)Convert.ChangeType(tempObj, typeof(T)));
                    T tempScript = tempObj.GetComponent<T>();
                    tempScript.KeyObject = go;
                    deactiveObjectsMap[go].Enqueue(tempScript);
                }
            }
        }
        internal T GetObjectWithKey(GameObject key)
        {
            if (deactiveObjectsMap.ContainsKey(key))
            {
                T obj = deactiveObjectsMap[key].Dequeue();

                if (!activeObjectsMap.ContainsKey(key))
                    activeObjectsMap.Add(key, new List<T>());
                activeObjectsMap[key].Add(obj);

                return obj;
            }
            return default(T);
        }
        internal void ReturnObject(GameObject key, T obj)
        {
            if (activeObjectsMap.ContainsKey(key))
            {
                if (activeObjectsMap[key].Contains(obj))
                {
                    GameObject tempObj = obj.ToGameObject();
                    tempObj.SetActive(false);
                    tempObj.transform.parent = TContainer;
                    tempObj.transform.localPosition = initialPos;

                    deactiveObjectsMap[key].Enqueue(obj);
                    activeObjectsMap[key].Remove(obj);
                }
            }
        }
        internal void ReturnAll()
        {
            if (activeObjectsMap.Keys.Count > 0)
            {
                foreach (GameObject go in activeObjectsMap.Keys)
                {
                    int count = activeObjectsMap[go].Count;
                    for (int i = 0; i < count; i++)
                    {
                        GameObject obj = activeObjectsMap[go][0].ToGameObject();
                        obj.SetActive(false);
                        obj.transform.parent = TContainer;
                        obj.transform.localPosition = initialPos;
                        deactiveObjectsMap[go].Enqueue(activeObjectsMap[go][0]);
                        activeObjectsMap[go].RemoveAt(0);
                    }
                }
            }
        }
    }
}
