﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class RendererExtensions
{
    static MaterialPropertyBlock propertyBlock = new MaterialPropertyBlock();
    static readonly int colorID = Shader.PropertyToID("_Color");
    static readonly int mainTextureID = Shader.PropertyToID("_MainTex");

    static Renderer tempRenderer;
    public static void ChangeColor(this Renderer renderer, Color value, int blockID = -1)
    {
        CheckRenderer(renderer);

        if (blockID != -1)
            propertyBlock.SetColor(blockID, value);
        else
            propertyBlock.SetColor(colorID, value);
        renderer.SetPropertyBlock(propertyBlock);
    }
    public static void ChangeColor(this Renderer renderer, ref MaterialPropertyBlock mPropertyBlock, Color value, int blockID = -1)
    {
        if (blockID != -1)
            mPropertyBlock.SetColor(blockID, value);
        else
            mPropertyBlock.SetColor(colorID, value);
        renderer.SetPropertyBlock(mPropertyBlock);
    }
    public static void ChangeTexture(this Renderer renderer, Texture value, Vector2 scale, int blockID = -1)
    {
        if (value != null)
        {
            CheckRenderer(renderer);

            if (blockID != -1)
                propertyBlock.SetTexture(blockID, value);
            else
                propertyBlock.SetTexture(mainTextureID, value);
            renderer.SetPropertyBlock(propertyBlock);
            if (blockID != -1)
                renderer.sharedMaterial.SetTextureScale(blockID, scale);
            else
                renderer.sharedMaterial.SetTextureScale(mainTextureID, scale);
        }
    }
    public static void ChangeValue(this Renderer renderer, float value, int blockID)
    {
        CheckRenderer(renderer);
        propertyBlock.SetFloat(blockID, value);

        renderer.SetPropertyBlock(propertyBlock);
    }
    public static void ChangeValue(this Renderer renderer, ref MaterialPropertyBlock mPropertyBlock, float value, int blockID)
    {
        mPropertyBlock.SetFloat(blockID, value);

        renderer.SetPropertyBlock(mPropertyBlock);
    }
    public static void ResetPropertyBlock(this Renderer renderer)
    {
        propertyBlock.Clear();
        renderer.SetPropertyBlock(propertyBlock);
    }
    static void CheckRenderer(Renderer renderer)
    {
        if (tempRenderer == null || renderer != tempRenderer)
        {
            propertyBlock.Clear();
            tempRenderer = renderer;
        }
    }
}
