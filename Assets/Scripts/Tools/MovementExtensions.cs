using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MovementExtensions
{
    public static void TimeMove(this MonoBehaviour monoBehaviour, Vector3 endPos, float duration, System.Action callback = null)
    {
        monoBehaviour.StartCoroutine(TimeMoveRoutine(monoBehaviour.transform, endPos, duration, callback));
    }
    static IEnumerator TimeMoveRoutine(Transform transform, Vector3 endPos, float duration, System.Action callback)
    {
        var dir = endPos - transform.position;
        var speed = dir.magnitude / duration;
        var timer = 0f;
        while (true)
        {
            transform.position += dir.normalized * speed * Time.deltaTime;
            timer += Time.deltaTime;
            if (timer >= duration)
            {
                transform.position = endPos;
                break;
            }
            yield return new WaitForEndOfFrame();
        }
        callback?.Invoke();
    }
    public static void SpeedMove(this MonoBehaviour monoBehaviour, Vector3 endPos, float speed, System.Action callback = null)
    {
        monoBehaviour.StartCoroutine(SpeedMoveRoutine(monoBehaviour.transform, endPos, speed, callback));
    }
    static IEnumerator SpeedMoveRoutine(Transform transform, Vector3 endPos, float speed, System.Action callback)
    {
        var dir = endPos - transform.position;
        var duration = dir.magnitude / speed;
        var timer = 0f;
        while (true)
        {
            transform.position += dir.normalized * speed * Time.deltaTime;
            timer += Time.deltaTime;
            if (timer >= duration)
            {
                transform.position = endPos;
                break;
            }
            yield return new WaitForEndOfFrame();
        }
        callback?.Invoke();
    }
    public static void LocalMove(this MonoBehaviour monoBehaviour, Vector3 endPos, float duration, System.Action callback = null)
    {
        monoBehaviour.StartCoroutine(LocalMoveRoutine(monoBehaviour.transform, endPos, duration, callback));
    }
    static IEnumerator LocalMoveRoutine(Transform transform, Vector3 endPos, float duration, System.Action callback)
    {
        var dir = endPos - transform.localPosition;
        var speed = dir.magnitude / duration;
        var timer = 0f;
        while (true)
        {
            transform.localPosition += dir.normalized * speed * Time.deltaTime;
            timer += Time.deltaTime;
            if (timer >= duration)
            {
                transform.localPosition = endPos;
                break;
            }
            yield return new WaitForEndOfFrame();
        }
        callback?.Invoke();
    }
    public static Coroutine Scale(this MonoBehaviour monoBehaviour, Vector3 endScale, float duration, System.Action callback = null)
    {
        return monoBehaviour.StartCoroutine(ScaleRoutine(monoBehaviour.transform, endScale, duration, callback));
    }
    static IEnumerator ScaleRoutine(Transform transform, Vector3 endScale, float duration, System.Action callback)
    {
        var dir = endScale - transform.localScale;
        var speed = dir.magnitude / duration;
        var timer = 0f;
        while (true)
        {
            transform.localScale += dir.normalized * speed * Time.deltaTime;
            timer += Time.deltaTime;
            if (timer >= duration)
            {
                transform.localScale = endScale;
                break;
            }
            yield return new WaitForEndOfFrame();
        }
        callback?.Invoke();
    }
}
