using System.Collections;
using System.Collections.Generic;
using TinyD3.ObjectPool;
using UnityEngine;

public class BubblePool : MonoBehaviour
{
    public static BubblePool Instance { get; private set; }

    private ObjectPool<Bubble> bubblePool;
    public int PoolSize = 100;
    public List<GameObject> BubblePrefabs;

    private void Awake()
    {
        Instance = this;

        CreateBubbles(BubblePrefabs);
    }

    internal void CreateBubbles(List<GameObject> bubbles)
    {
        if (bubbles != null && bubbles.Count > 0)
        {
            if (bubblePool == null)
                bubblePool = new ObjectPool<Bubble>(transform, bubbles, PoolSize);
            else
            {
                bubblePool.CreateObjects(bubbles, PoolSize);
            }
        }
    }

    public Bubble GetBubble(GameObject keyObject = null)
    {
        if (keyObject)
            return bubblePool.GetObjectWithKey(keyObject);
        return bubblePool.GetObjectWithKey(BubblePrefabs[Random.Range(0, BubblePrefabs.Count)]);
    }

    public void ReturnBubble(Bubble bubble)
    {
        bubble.SetStatus(false);
        bubblePool.ReturnObject(bubble.KeyObject, bubble);
    }
    public void ReturnAll()
    {
        bubblePool.ReturnAll();
    }
}
