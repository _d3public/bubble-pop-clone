using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleGenerator : MonoBehaviour
{
    public float BubbleSize { get; private set; }

    private float rowXOffset;
    private float colYOffset;
    private int bubbleCountInRow;
    private int initialRowCount;
    private float offsetBetweenBubbles;
    private bool isShifted;
    private void Awake()
    {
        rowXOffset = GameManager.Instance.GameValues.RowXOffset;
        colYOffset = GameManager.Instance.GameValues.ColYOffset;
        bubbleCountInRow = GameManager.Instance.GameValues.BubbleCountInRow;
        initialRowCount = GameManager.Instance.GameValues.InitialRowCount;
        offsetBetweenBubbles = GameManager.Instance.GameValues.OffsetBetweenBubbles;
    }
    internal void CreateGameArea()
    {
        for (int i = 0; i < initialRowCount; i++)
        {
            CreateNewRow(i);
        }

        GameLogic.Instance.DelayedScaleOfBubble();
    }
    void CreateNewRow(int level = 0)
    {
        Bubble[] bubbleRow = new Bubble[bubbleCountInRow];
        for (int i = 0; i < bubbleCountInRow; i++)
        {
            Bubble b = BubblePool.Instance.GetBubble();
            BubbleSize = b.BubbleSize.x;
            var bubblePos = GetPosition(level, i);

            b.transform.parent = this.transform;
            b.transform.localPosition = bubblePos;
            b.transform.localScale = Vector3.zero;
            b.SetBubbleID(Random.Range(LevelManager.Instance.CurrentLevelValues.GameAreaMinBubble, LevelManager.Instance.CurrentLevelValues.GameAreaMaxBubble));
            b.SetStatus(true);
            b.gameObject.SetActive(true);

            bubbleRow[i] = b;
        }
        GameLogic.Instance.InsertBubbleRow(bubbleRow, level);
    }
    internal void InsertNewRow()
    {
        isShifted = !isShifted;
        CreateNewRow();
    }
    internal Vector3 GetPosition(int colIndex, int rowIndex, int upCount = 0)
    {
        Vector3 tempPos = Vector3.zero;
        tempPos.x = (-bubbleCountInRow * BubbleSize) / 2f + (BubbleSize / 2f) - (offsetBetweenBubbles * (bubbleCountInRow - 1)) * 2;
        tempPos.x += rowIndex * BubbleSize + rowIndex * offsetBetweenBubbles;
        tempPos.x += colIndex % 2 == 0 ? isShifted ? 0f : rowXOffset : isShifted ? rowXOffset : 0f;
        tempPos.y += (colIndex - upCount) * -BubbleSize - offsetBetweenBubbles - colYOffset;
        return tempPos;
    }
}
