using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GameLogic : MonoBehaviour
{
    public static GameLogic Instance { get; private set; }

    [SerializeField] private BubbleGenerator bubbleGenerator;
    [SerializeField] private int minRootLevelCount = 6;
    [SerializeField] private int maxRootLevelCount = 8;

    private List<Bubble[]> activeBubbles = new List<Bubble[]>();
    private int upCount = 0;
    private Dictionary<string, (int, int)> availablePositionsMap;
    private List<Vector3> potentialObstacleBubbles;
    private Vector2 minMaxHeight;

    public static event System.Action onShiftDown;
    public static event System.Action onShiftUp;
    private void OnEnable()
    {
        GameManager.onGameStarted += Handle_onGameStarted;
    }
    private void OnDisable()
    {
        GameManager.onGameStarted -= Handle_onGameStarted;
    }
    private void Awake()
    {
        Instance = this;
    }
    private void Handle_onGameStarted()
    {
        minMaxHeight = Vector2.right * 99f;
        bubbleGenerator.CreateGameArea();
    }
    void ShiftUp()
    {
        onShiftUp?.Invoke();
        upCount++;
    }
    void ShiftDown()
    {
        onShiftDown?.Invoke();
        if (upCount == 0)
        {
            bubbleGenerator.InsertNewRow();
            DelayedScaleOfBubble();
        }
        else
            upCount--;
    }
    void CheckCurrentRootLevel()
    {
        var tempBubbles = activeBubbles.Where(bArray => bArray.Count(b => b != null) > 0).ToList();
        activeBubbles.Clear();
        activeBubbles = tempBubbles;
        var currentRootLevel = activeBubbles.Count;

        if (currentRootLevel - upCount > maxRootLevelCount)
            ShiftUp();
        else if (currentRootLevel < minRootLevelCount)
            ShiftDown();
    }
    public void InsertBubbleRow(Bubble[] bubbleRow, int level)
    {
        activeBubbles.Insert(level, bubbleRow);

        for (int i = 0; i < bubbleRow.Length; i++)
        {
            bubbleRow[i].SetCoord(new Vector2Int(level, i));
            CheckAroundOfBubble(bubbleRow[i]);
        }
    }
    public void DelayedScaleOfBubble()
    {
        System.Random random = new System.Random();
        List<Bubble> bubbles = new List<Bubble>();
        foreach (Bubble[] bArray in activeBubbles)
        {
            for (int i = 0; i < bArray.Length; i++)
            {
                if (bArray[i] != null)
                    bubbles.Add(bArray[i]);
            }
        }
        Bubble[] shuffledBubbles = bubbles.Where(b => b.transform.localScale.magnitude == 0).OrderBy(r => random.Next()).ToArray();
        StartCoroutine(DelayedScaleRoutine(shuffledBubbles));
    }
    IEnumerator DelayedScaleRoutine(Bubble[] shuffledBubbles)
    {
        int bubbleCount = shuffledBubbles.Length;
        for (int i = 0; i < bubbleCount; i++)
        {
            shuffledBubbles[i].Scale(Vector3.one, .1f);
            yield return new WaitForEndOfFrame();
        }
    }
    void CheckAroundOfBubble(Bubble bubble)
    {
        List<Bubble> bubbles = new List<Bubble>();
        foreach (Bubble[] bArray in activeBubbles)
        {
            for (int i = 0; i < bArray.Length; i++)
            {
                if (bArray[i] != null && bArray[i] != bubble)
                    bubbles.Add(bArray[i]);
            }
        }
        Bubble[] neighbours = bubbles.Where(b => Vector3.Distance(b.transform.position, bubble.transform.position) <= (bubble.BubbleSize.x + .15f) /*&& Mathf.Abs(bubble.Coord.x - b.Coord.x) <= 1*/).ToArray();
        foreach (Bubble b in neighbours)
        {
            bubble.AddNeighbour(b);
        }
        if (neighbours != null && neighbours.Length == 0)
        {
            bubble.Fall();
        }
            
    }
    int GetAroundOfBubbleCount(Vector3 position)
    {
        List<Bubble> bubbles = new List<Bubble>();
        foreach (Bubble[] bArray in activeBubbles)
        {
            for (int i = 0; i < bArray.Length; i++)
            {
                if (bArray[i] != null)
                    bubbles.Add(bArray[i]);
            }
        }
        Bubble[] neighbours = bubbles.Where(b => Vector3.Distance(b.transform.position, position) <= (b.BubbleSize.x + .1f)).ToArray();
        return neighbours.Length;
    }
    internal void AddBubble(Bubble bubble)
    {
        var bubblePos = bubble.transform.position;
        var coords = availablePositionsMap[bubblePos.ToString()];
        if (coords.Item1 == activeBubbles.Count)
        {
            int rowLength = 0;
            if (activeBubbles.Count > 0)
                rowLength = activeBubbles[0].Length;
            activeBubbles.Add(new Bubble[rowLength]);
        }
        activeBubbles[coords.Item1][coords.Item2] = bubble;
        bubble.SetStatus(true);
        bubble.SetCoord(new Vector2Int(coords.Item1, coords.Item2));
        bubble.transform.parent = bubbleGenerator.transform;
        CheckAroundOfBubble(bubble);
        var isMerged = CheckMergeability(bubble);
        if (!isMerged)
            CheckCurrentRootLevel();
        minMaxHeight = Vector2.right * 99f;
    }
    internal void RemoveBubble(Bubble bubble)
    {
        var coords = bubble.Coord;
        if (activeBubbles.Count > coords.x)
        {
            activeBubbles[coords.x][coords.y] = null;
            int rowCount = activeBubbles[coords.x].Length;
            bool isEmpty = true;

            for (int i = 0; i < rowCount; i++)
            {
                if (activeBubbles[coords.x][i] != null)
                    isEmpty = false;
            }
            if (isEmpty)
            {
                int diff = (activeBubbles.Count - 1) - coords.x;
                for (int i = 0; i < diff; i++)
                {
                    if (activeBubbles[activeBubbles.Count - 1].Count(b => b != null) > 0)
                    {
                        foreach (var b in activeBubbles[activeBubbles.Count - 1])
                        {
                            if (b != null)
                                b.Fall();
                        }
                    }
                    activeBubbles.RemoveAt(activeBubbles.Count - 1);
                }
            }
        }
    }
    bool CheckMergeability(Bubble bubble)
    {
        var mergedBubbles = bubble.GetSameIDNeighbors();
        if (mergedBubbles.Count > 0)
        {
            mergedBubbles = mergedBubbles.OrderByDescending(mB => mB.transform.position.y).ToList();
            Bubble targetBubble = mergedBubbles[0];
            if (bubble.transform.position.y >= targetBubble.transform.position.y)
            {
                if (bubble.transform.position.y == targetBubble.transform.position.y)
                {
                    if (bubble.NeighborCount > targetBubble.NeighborCount)
                        targetBubble = bubble;
                }
                else
                {
                    targetBubble = bubble;
                }
            }
            if (targetBubble != bubble)
            {
                mergedBubbles.Add(bubble);
                mergedBubbles.Remove(targetBubble);
            }
            int bubbleCount = mergedBubbles.Count;
            int finalID = bubble.BubbleID + bubbleCount;
            bool canExplodeNeighbors = false;
            var mergeTime = .1f;

            for (int i = 0; i < bubbleCount; i++)
            {
                var tempBubble = mergedBubbles[0];
                mergedBubbles.RemoveAt(0);
                tempBubble.SetStatus(false);
                tempBubble.ActivateExplodeParticle();

                tempBubble.TimeMove(targetBubble.transform.position, mergeTime, () =>
                {
                    tempBubble.Explode();
                });
            }

            targetBubble.DelayedCall(mergeTime + .05f, () =>
               {
                   canExplodeNeighbors = targetBubble.SetBubbleID(finalID);
                   targetBubble.IncreaseScore();
                   targetBubble.SetStatus(true);
                   CheckAroundOfBubble(targetBubble);
                   if (canExplodeNeighbors)
                   {
                       targetBubble.DelayedCall(.5f, () =>
                       {
                           var targetNeighbors = targetBubble.GetNeighbors();

                           targetBubble.SetStatus(false);
                           targetBubble.ActivateExplodeParticle();
                           targetBubble.Explode();
                           if (targetNeighbors != null)
                           {
                               var neighborCount = targetNeighbors.Count;

                               if (neighborCount > 0)
                                   for (int i = 0; i < neighborCount; i++)
                                   {
                                       targetNeighbors[0].SetStatus(false);
                                       targetNeighbors[0].ActivateExplodeParticle();
                                       targetNeighbors[0].Explode();
                                       targetNeighbors.RemoveAt(0);
                                   }
                           }
                           CheckCurrentRootLevel();
                       });
                   }
                   else
                   {
                       var isMerged = CheckMergeability(targetBubble);
                       if (!isMerged)
                           CheckCurrentRootLevel();
                   }
               });
            return true;
        }
        return false;
    }
    internal Vector3[] GetAvailablePlaces()
    {
        List<Vector3> availablePlaces = new List<Vector3>();
        availablePositionsMap = new Dictionary<string, (int, int)>();
        potentialObstacleBubbles = new List<Vector3>();

        int currentRootLevel = activeBubbles.Count;
        int rowLength = 0;
        if (currentRootLevel > 0)
            rowLength = activeBubbles[0].Length;
        for (int i = currentRootLevel; i >= 0; i--)
        {
            if (i == currentRootLevel)
            {
                for (int j = 0; j < rowLength; j++)
                {
                    var tempPos = bubbleGenerator.GetPosition(i, j, upCount);
                    tempPos += bubbleGenerator.transform.localPosition;

                    if (GetAroundOfBubbleCount(tempPos) > 0)
                    {
                        availablePositionsMap.Add(tempPos.ToString(), (i, j));
                        availablePlaces.Add(tempPos);
                        var heightValue = tempPos.y - bubbleGenerator.BubbleSize / 2;
                        if (heightValue / 4 - bubbleGenerator.BubbleSize <= minMaxHeight.x)
                            minMaxHeight.x = heightValue / 4 - bubbleGenerator.BubbleSize;
                        if (heightValue > minMaxHeight.y)
                            minMaxHeight.y = tempPos.y - bubbleGenerator.BubbleSize / 2;
                    }
                }
            }
            else
            {
                bool hasNull = false;
                for (int j = 0; j < rowLength; j++)
                {
                    if (activeBubbles[i][j] == null)
                    {
                        hasNull = true;
                        var tempPos = bubbleGenerator.GetPosition(i, j, upCount);
                        tempPos += bubbleGenerator.transform.localPosition;

                        if (GetAroundOfBubbleCount(tempPos) > 0)
                        {
                            availablePositionsMap.Add(tempPos.ToString(), (i, j));
                            availablePlaces.Add(tempPos);
                            var heightValue = tempPos.y - bubbleGenerator.BubbleSize / 2;
                            if (heightValue / 4 - bubbleGenerator.BubbleSize <= minMaxHeight.x)
                                minMaxHeight.x = heightValue / 4 - bubbleGenerator.BubbleSize;
                            if (heightValue > minMaxHeight.y)
                                minMaxHeight.y = heightValue;
                        }
                    }
                    else
                    {
                        potentialObstacleBubbles.Add(activeBubbles[i][j].transform.position);
                    }
                }
                if (potentialObstacleBubbles.Count > 0)
                {
                    var minObstackleHeight = potentialObstacleBubbles.OrderBy(b => b.y).ToArray()[0];
                    minMaxHeight.y = (minMaxHeight.y + minObstackleHeight.y) / 2f;
                }
                if (!hasNull)
                {
                    return availablePlaces.OrderByDescending(p => p.y).ToArray();
                }
            }
        }
        return availablePlaces.OrderByDescending(p => p.y).ToArray();
    }
    internal Vector3[] GetPotentialObstaclePositions(Vector3 rayOrigin, Vector3 targetPosition)
    {
        return potentialObstacleBubbles.Where(p => Vector3.Distance(rayOrigin, p) < Vector3.Distance(rayOrigin, targetPosition)).ToArray();
    }
    internal Vector2 GetMinMaxHeight()
    {
        return minMaxHeight;
    }
}
