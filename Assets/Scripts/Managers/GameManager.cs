using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    public GameValues GameValues;

    public static event System.Action onGameStarted;
    public static event System.Action onGameEnded;
    private void OnEnable()
    {
        LevelManager.onScoreUpdate += Handle_onScoreUpdate;
        LevelManager.onLevelUpdate += Handle_onLevelUpdate;
    }
    private void OnDisable()
    {
        LevelManager.onScoreUpdate -= Handle_onScoreUpdate;
        LevelManager.onLevelUpdate -= Handle_onLevelUpdate;
    }
    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        StartGame();
    }
    void StartGame()
    {
        onGameStarted?.Invoke();
    }
    private void Handle_onScoreUpdate(float score, float percentage)
    {
        UIManager.Instance.UpdateScore(score);
        UIManager.Instance.UpdateLevelBar(percentage, true);
    }
    private void Handle_onLevelUpdate(int currentLevel)
    {
        UIManager.Instance.SetLevelInfo(currentLevel);
    }
}
