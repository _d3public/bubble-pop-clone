using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance { get; private set; }
    public LevelData LevelData;
    public LevelDataValues CurrentLevelValues => LevelData.LevelDataValues.Count <= CurrentLevel ? LevelData.LevelDataValues[LevelData.LevelDataValues.Count - 1] : LevelData.LevelDataValues[CurrentLevel];
    public int CurrentLevel
    {
        get => PlayerPrefs.GetInt("CurrentLevel", 0);
        set => PlayerPrefs.SetInt("CurrentLevel", value);
    }
    public float TotalScore
    {
        get => PlayerPrefs.GetFloat("TotalScore", 0f);
        set => PlayerPrefs.SetFloat("TotalScore", value);
    }
    private float targetScore => LevelData.TargetLevelScores.Length <= CurrentLevel ? LevelData.TargetLevelScores[LevelData.TargetLevelScores.Length - 1] * (CurrentLevel - LevelData.TargetLevelScores.Length - 1) * 1.5f : LevelData.TargetLevelScores[CurrentLevel];
    public static event System.Action<float, float> onScoreUpdate;
    public static event System.Action<int> onLevelUpdate;
    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        onScoreUpdate?.Invoke(TotalScore, GetLevelPercentage());
        onLevelUpdate?.Invoke(CurrentLevel);
    }
    internal float GetLevelPercentage()
    {
        var prevScore = 0f;
        if (CurrentLevel > 0)
        {
            prevScore = LevelData.TargetLevelScores.Length <= CurrentLevel ? LevelData.TargetLevelScores[LevelData.TargetLevelScores.Length - 2] : LevelData.TargetLevelScores[CurrentLevel - 1];
        }
        return Mathf.InverseLerp(prevScore, targetScore, TotalScore);
    }
    public void AddScore(float score)
    {
        TotalScore += score;
        CheckForLeveling();
        onScoreUpdate?.Invoke(TotalScore, GetLevelPercentage());
    }

    private void CheckForLeveling()
    {
        if (TotalScore >= targetScore)
            IncreaseLevel();
    }

    void IncreaseLevel()
    {
        CurrentLevel++;
        onLevelUpdate?.Invoke(CurrentLevel);
    }
}
