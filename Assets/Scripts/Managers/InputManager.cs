using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputManager : MonoBehaviour
{
    Touch controllerTouch;
    int touchId = 0;
    Vector3 touchPos;

    public static event System.Action onInputDown;
    public static event System.Action onInputUp;
    public static event System.Action<Vector2> onInputMove;
    private void Update()
    {
        CheckInput();
    }
    void CheckInput()
    {
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
#else
        if (Input.touchCount > 0)
        {
            foreach (Touch t in Input.touches)
            {
                if (t.fingerId == touchId)
                {
                    controllerTouch = t;
                    break;
                }
            }
        }

        if (controllerTouch.phase == TouchPhase.Began && !EventSystem.current.IsPointerOverGameObject(touchId))
#endif
        {
            var downPos = Vector3.zero;
#if UNITY_EDITOR
            downPos = Input.mousePosition;
#else
            downPos = controllerTouch.position;
#endif
            Vector2 initialTouchPos = Camera.main.ScreenToWorldPoint(downPos);
            touchPos = initialTouchPos;
            onInputDown?.Invoke();
        }
#if UNITY_EDITOR
        if (Input.GetMouseButton(0))
#else
        if ((controllerTouch.phase == TouchPhase.Moved || controllerTouch.phase == TouchPhase.Stationary))
#endif
        {
#if UNITY_EDITOR
            Vector2 mousePos = Input.mousePosition;
#else
            Vector2 mousePos = /*Input.mousePosition*/ controllerTouch.position;
#endif
            Vector2 realPos = Camera.main.ScreenToWorldPoint(mousePos);

            var lerpSpeed = 10f;
            touchPos = Vector3.Lerp(touchPos, realPos, lerpSpeed * Time.deltaTime);

            onInputMove?.Invoke(touchPos);
        }

#if UNITY_EDITOR
        if (Input.GetMouseButtonUp(0))
#else
        if ((controllerTouch.phase == TouchPhase.Ended || controllerTouch.phase == TouchPhase.Canceled))
#endif
        {
            var upPos = Vector3.zero;
#if UNITY_EDITOR
            upPos = Input.mousePosition;
#else
            upPos = controllerTouch.position;
#endif
            onInputUp?.Invoke();
        }
    }
}
